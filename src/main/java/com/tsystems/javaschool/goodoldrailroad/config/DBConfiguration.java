package com.tsystems.javaschool.goodoldrailroad.config;

import org.postgresql.ds.PGPoolingDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DBConfiguration {

    @Bean
    public DataSource dataSource() throws SQLException {
        PGPoolingDataSource source = new PGPoolingDataSource();
        source.setServerName("localhost");
        source.setDatabaseName("db_goodrailroad");
        source.setUser("postgres");
        source.setPassword("postgres");
        source.setMaxConnections(0);        // 0 - no maximum
        Connection con = null;
        try {
            con = source.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return source;
    }

    @Bean
    public LocalSessionFactoryBean localSessionFactoryBean(DataSource dataSource) {
        LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
        sfb.setDataSource(dataSource);
        Properties props = new Properties();
        sfb.setPackagesToScan("com.tsystems.javaschool.goodoldrailroad.model");
        props.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        props.setProperty("current_session_context_class", "thread");
        props.setProperty("cache.provider_class", "org.hibernate.cache.NoCacheProvider");
        props.setProperty("hibernate.connection.SetBigStringTryClob", "true");
        props.setProperty("hibernate.jdbc.batch_size", "0");
        props.setProperty("hibernate.hbm2ddl.auto", "update");
        props.setProperty("show_sql", "true");
        props.setProperty("format_sql", "true");
        sfb.setHibernateProperties(props);
        return sfb;
    }

    @Bean
    public HibernateTransactionManager hibernateTransactionManager(LocalSessionFactoryBean localSessionFactoryBean) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(localSessionFactoryBean.getObject());
        return transactionManager;
    }
}

