package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.Route;
import com.tsystems.javaschool.goodoldrailroad.model.Station;
import com.tsystems.javaschool.goodoldrailroad.model.StationOnRoute;
import java.util.List;

//check if needed at all
public interface StationOnRouteService {
    List<Station> getStationsOnRoute(int routeId);
    List<Route> getRoutesForStation(int stationId);
    List<StationOnRoute> getAll();
    void addList(List<Station> stations, Route route);
    void delete(StationOnRoute stationOnRoute);
    void update(StationOnRoute stationOnRoute);
    List<StationOnRoute> getAllOnRoute(int routeId);
}