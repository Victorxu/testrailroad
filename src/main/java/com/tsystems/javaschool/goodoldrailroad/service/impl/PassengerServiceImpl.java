package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.PassengerDao;
import com.tsystems.javaschool.goodoldrailroad.model.Passenger;
import com.tsystems.javaschool.goodoldrailroad.service.api.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService {

    @Autowired
    private PassengerDao passengerDao;

    public Passenger getPassenger(int id) {
        return passengerDao.getPassenger(id);
    }

    public List<Passenger> getAllPassengers() {
        return passengerDao.getAllPassengers();
    }

    public void addPassenger(Passenger passenger) {
        passengerDao.addPassenger(passenger);
    }

    public void deletePassenger(Passenger passenger) {
        passengerDao.deletePassenger(passenger);
    }

    public void updatePassenger(Passenger passenger) {
        passengerDao.updatePassenger(passenger);
    }
}