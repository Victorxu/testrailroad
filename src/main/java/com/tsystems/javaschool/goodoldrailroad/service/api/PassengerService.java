package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.Passenger;
import java.util.List;

public interface PassengerService {
    Passenger getPassenger(int id);
    List<Passenger> getAllPassengers();
    void addPassenger(Passenger passenger);
    void deletePassenger(Passenger passenger);
    void updatePassenger(Passenger passenger);
}