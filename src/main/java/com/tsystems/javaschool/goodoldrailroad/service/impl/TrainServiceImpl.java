package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.TrainDao;
import com.tsystems.javaschool.goodoldrailroad.model.Train;
import com.tsystems.javaschool.goodoldrailroad.service.api.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TrainServiceImpl implements TrainService {

    private final TrainDao trainDao;

    @Autowired
    public TrainServiceImpl(TrainDao trainDao) {
        this.trainDao = trainDao;
    }

    public Train getTrain(int id) {
        return trainDao.getTrain(id);
    }

    public List<Train> getAllTrains() {
        return trainDao.getAllTrains();
    }

    @Transactional
    public void addTrain(Train train) {
        if (train.getCapacity() > 0) {
            trainDao.addTrain(train);
        } else {
            // add exception
            return;
        }
    }

    @Transactional
    public void addTrain(int capacity) {
        if (capacity > 0) {
            Train train = new Train();
            train.setCapacity(capacity);
            addTrain(train);
        } else {
            // add exception
            return;
        }
    }

    public void updateTrain(Train train) {
        trainDao.updateTrain(train);
    }
}