package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.LineDao;
import com.tsystems.javaschool.goodoldrailroad.dao.api.StationDao;
import com.tsystems.javaschool.goodoldrailroad.dao.api.StationOnRouteDao;
import com.tsystems.javaschool.goodoldrailroad.model.*;
import com.tsystems.javaschool.goodoldrailroad.service.api.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.util.*;

@Service
public class StationServiceImpl implements StationService {

    private final StationDao stationDao;
    private final LineDao lineDao;
    private final StationOnRouteDao stationOnRouteDao;

    @Autowired
    public StationServiceImpl(StationDao stationDao, LineDao lineDao, StationOnRouteDao stationOnRouteDao) {
        this.stationDao = stationDao;
        this.lineDao = lineDao;
        this.stationOnRouteDao = stationOnRouteDao;
    }

    public Station getStationById(int id) {
        return stationDao.getStation(id);
    }

    public List<Station> getAllStations() {
        return stationDao.getAllStations();
    }

    public List<Station> getStationsByLine(int lineId) {
        return stationDao.getStationsOnLine(lineId);
    }

    public void addStation(Station station) {
        stationDao.addStation(station);
    }

    @Transactional
    public void addStation(String name, int lineId, boolean isTransit) {

        if (null == name) {
            // add exception
            return;
        }

        if ("".equals(name)) {
            // add exception
            return;
        }

        Line line = lineDao.getLine(lineId);
        if (null == line) {
            // add exception
            return;
        }

        List<Station> existingStations = this.getAllStations();
        for (Station st : existingStations) {
            if (name.equals(st.getName())) {
                // add exception
                return;
            }
        }

        Station newStation = new Station();
        newStation.setName(name);
        newStation.setTransit(isTransit);
        newStation.setLine(line);

        this.addStation(newStation);
    }

    public void deleteStation(Station station) {
        stationDao.deleteStation(station);
    }

    public void updateStation(Station station) {
        stationDao.updateStation(station);
    }

    @Transactional
    public List<TimetableRow> getScheduleForStation(int stationId) {

        List<TimetableRow> timetable = new ArrayList<>();
        Station station = stationDao.getStation(stationId);
        List<Route> routes = station.getRoutes();


        for (Route r : routes) {
            List<Voyage> voyages = r.getVoyages();
            for (Voyage v : voyages) {
                TimetableRow ttu = new TimetableRow();
                ttu.setRouteName(r.getName());
                ttu.setVoyageId(v.getId());
                Time timeFromRouteStart = new Time(0);
                StationOnRoute stor = stationOnRouteDao.get(stationId, r.getId());
                timeFromRouteStart.setTime(timeFromRouteStart.getTime() + stor.getArrivalTime().getTime());
                Date startTime = new Date(v.getStartTime().getTime() + timeFromRouteStart.getTime());

                //check if train should have left in past
                if (startTime.before(new Date())) {
                    //date of voyage is in the past
                    continue;
                }

                //and change later - +3hours problem   // solved  ?
                ttu.setDepartureTime(startTime);
                timetable.add(ttu);
            }
        }

        Collections.sort(timetable, Comparator.comparing(TimetableRow::getDepartureTime));
        return timetable;
    }
}