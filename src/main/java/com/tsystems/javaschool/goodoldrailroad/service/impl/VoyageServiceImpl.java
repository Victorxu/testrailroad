package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.model.*;
import com.tsystems.javaschool.goodoldrailroad.dao.api.*;
import com.tsystems.javaschool.goodoldrailroad.model.Voyage;
import com.tsystems.javaschool.goodoldrailroad.service.api.VoyageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class VoyageServiceImpl implements VoyageService {

    private final VoyageDao voyageDao;
    private final TrainDao trainDao;
    private final StationDao stationDao;
    private final StationOnRouteDao stationOnRouteDao;

    @Autowired
    public VoyageServiceImpl(VoyageDao voyageDao, TrainDao trainDao, StationDao stationDao, StationOnRouteDao stationOnRouteDao) {
        this.voyageDao = voyageDao;
        this.trainDao = trainDao;
        this.stationDao = stationDao;
        this.stationOnRouteDao = stationOnRouteDao;
    }

    public Voyage getVoyage(int id) {
        return voyageDao.getVoyage(id);
    }

    public List<Voyage> getAllVoyages() {
        return voyageDao.getAllVoyages();
    }

    // сделать такие же проверки, как и в методе с другими параметрами
    @Transactional
    public void addVoyage(Voyage voyage) {
        if ((null == voyage.getRoute()) || (null == voyage.getTrain()) || (null == voyage.getStartTime())
                || (voyage.getStartTime().before(new Date()))) {
            // add exception
            return;
        }

        // check if such train already exists in db
        // добавить бы проверку, что поезд в базе действительно такой, как передан в метод
        if (null == trainDao.getTrain(voyage.getTrain().getId())) {
            //no such train in db
            // add exception
            return;
        }

        //костыль,должен бы отрабатывать @ColumnDefault
        voyage.setStatus("On schedule");

        voyageDao.addVoyage(voyage);
    }

    public void deleteVoyage(Voyage voyage) {
        voyageDao.deleteVoyage(voyage);
    }

    public void updateVoyage(Voyage voyage) {
        voyageDao.updateVoyage(voyage);
    }

    //change later for Ring routes
    @Transactional
    public List<Voyage> getVoyagesFromTo(int stationAId, int stationBId, Date fromTime, Date toTime) {
        if ((stationAId <= 0) || (stationBId <= 0) ||
                (stationAId == stationBId) || (fromTime.getTime() > toTime.getTime())) {
            return new ArrayList<>();
        }

        List<Voyage> voyagesFromAToB = new ArrayList<>();
        List<Voyage> voyagesFromAToBInInterval = new ArrayList<>();
        Station stationA = stationDao.getStation(stationAId);
        Station stationB = stationDao.getStation(stationBId);
        List<Route> routesWithA = stationA.getRoutes();
        List<Route> routesWithB = stationB.getRoutes();
        List<Route> routesWithAAndB = routesWithA;
        List<Route> routesFromAToB = new ArrayList<>();

        routesWithAAndB.retainAll(routesWithB);

        // filter routes from A to B only
        for (Route r : routesWithAAndB) {
            List<StationOnRoute> stationOnRouteList = stationOnRouteDao.getAllOnRoute(r.getId());
            int stAIdOnRoute = 0;
            int stBIdOnRoute = 0;
            for (StationOnRoute st : stationOnRouteList) {
                if (st.getStation().getId() == stationAId) {
                    stAIdOnRoute = st.getStationIndexInRoute();
                }
            }
            for (StationOnRoute st : stationOnRouteList) {
                if (st.getStation().getId() == stationBId) {
                    stBIdOnRoute = st.getStationIndexInRoute();
                }
            }
            if ((stBIdOnRoute > stAIdOnRoute) && (stAIdOnRoute != 0) && (stBIdOnRoute != 0)) {
                routesFromAToB.add(r);
            }
        }

        for (Route r : routesFromAToB) {
            voyagesFromAToB.addAll(r.getVoyages());
        }

        // checking for time interval
        for (Voyage v : voyagesFromAToB) {
            StationOnRoute stor = stationOnRouteDao.get(stationAId, v.getRoute().getId());
            Date arrivalTime = stor.getArrivalTime();
            Date startTimeFromStationA = new Date(v.getStartTime().getTime() + arrivalTime.getTime());
            if ((fromTime.getTime() <= startTimeFromStationA.getTime())
                    && (toTime.getTime() >= startTimeFromStationA.getTime())) {
                voyagesFromAToBInInterval.add(v);
            }
        }

        Collections.sort(voyagesFromAToBInInterval, Comparator.comparing(Voyage::getStartTime));
        return voyagesFromAToBInInterval;
    }
}
