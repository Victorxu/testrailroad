package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.RouteDao;
import com.tsystems.javaschool.goodoldrailroad.dao.api.StationOnRouteDao;
import com.tsystems.javaschool.goodoldrailroad.model.Route;
import com.tsystems.javaschool.goodoldrailroad.model.Station;
import com.tsystems.javaschool.goodoldrailroad.service.api.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class RouteServiceImpl implements RouteService {

    private final RouteDao routeDao;
    private final StationOnRouteDao stationOnRouteDao;

    @Autowired
    public RouteServiceImpl(RouteDao routeDao, StationOnRouteDao stationOnRouteDao) {
        this.routeDao = routeDao;
        this.stationOnRouteDao = stationOnRouteDao;
    }

    public Route getRoute(int id) {
        return routeDao.getRoute(id);
    }

    public Route getRoute(String name) {
        return routeDao.getRoute(name);
    }

    public List<Route> getAllRoutes() {
        return routeDao.getAllRoutes();
    }

    @Transactional
    public void addRoute(Route route) {

        if ((null == route.getStationsOnRoute()) || ("".equals(route.getName()))) {
            // add exception
            return;
        }

        List<Route> existingRoutes = this.getAllRoutes();
        for (Route r : existingRoutes) {
            if (r.getName().equals(route.getName())) {
                // add exception
                return;
            }
        }

        //change later - to manually entered value
        long totalDuration = 60000L * 5 * (route.getStationsOnRoute().size() - 1);
        route.setDuration(new Time(totalDuration));


        // sorting stations
        List<Station> stations = route.getStations();

        //change later for right kind of sort .reversed
        Collections.sort(stations, Comparator.comparing(Station::getId));
        route.setStations(stations);

        routeDao.addRoute(route);
        stationOnRouteDao.addList(route.getStationsOnRoute());
    }

    @Transactional
    public void addRoute(String name, List<Station> stations) {             //fucking Time and UTC in DB +3hours

        if ((null == stations) || ("".equals(name)) || (null == name)) {
            // add exception
            return;
        }

        List<Route> existingRoutes = this.getAllRoutes();
        for (Route r : existingRoutes) {
            if (r.getName().equals(name)) {
                // add exception
                return;
            }
        }

        Route route = new Route();
        route.setName(name);

        //change later - to manually entered value
        long totalDuration = 60000L * 5 * (stations.size() - 1);

        // time, localtime, etc - incorect, 3hours problem
        route.setDuration(new Time(totalDuration));

        // sorting stations
        //change later for right kind of sort .reversed
        stations.sort(Comparator.comparing(Station::getId));

        routeDao.addRoute(route);
        stationOnRouteDao.addList(stations, route);
    }

    public void deleteRoute(Route route) {
        routeDao.deleteRoute(route);
    }

    public void updateRoute(Route route) {
        routeDao.updateRoute(route);
    }
}