package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.Route;
import com.tsystems.javaschool.goodoldrailroad.model.Station;

import java.util.List;

public interface RouteService {
    Route getRoute(int id);
    Route getRoute(String name);
    List<Route> getAllRoutes();
    void addRoute(Route route);
    void addRoute(String name, List<Station> stations);
    void deleteRoute(Route route);
    void updateRoute(Route route);
}