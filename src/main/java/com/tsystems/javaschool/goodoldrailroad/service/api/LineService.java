package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.Line;
import java.util.List;

public interface LineService {
    Line getLine(int id);
    List<Line> getAllLines();
    void addLine(Line line);
    void deleteLine(Line line);
    void updateLine(Line line);
}
