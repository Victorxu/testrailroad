package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.StationOnRouteDao;
import com.tsystems.javaschool.goodoldrailroad.model.Route;
import com.tsystems.javaschool.goodoldrailroad.model.Station;
import com.tsystems.javaschool.goodoldrailroad.model.StationOnRoute;
import com.tsystems.javaschool.goodoldrailroad.service.api.StationOnRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//check if needed at all
@Service
public class StationOnRouteServiceImpl implements StationOnRouteService {

    private final StationOnRouteDao stationOnRouteDao;

    @Autowired
    public StationOnRouteServiceImpl(StationOnRouteDao stationOnRouteDao) {
        this.stationOnRouteDao = stationOnRouteDao;
    }

    public List<Station> getStationsOnRoute(int routeId) {
        return stationOnRouteDao.getStationsOnRoute(routeId);
    }

    public List<Route> getRoutesForStation(int stationId) {
        return stationOnRouteDao.getRoutesForStation(stationId);
    }

    public List<StationOnRoute> getAll() {
        return stationOnRouteDao.getAll();
    }

    public void addList(List<Station> stations, Route route) {
        stationOnRouteDao.addList(stations, route);
    }

    public void delete(StationOnRoute stationOnRoute) {
        stationOnRouteDao.delete(stationOnRoute);
    }

    public void update(StationOnRoute stationOnRoute) {
        stationOnRouteDao.update(stationOnRoute);
    }

    public List<StationOnRoute> getAllOnRoute(int routeId) {
        return stationOnRouteDao.getAllOnRoute(routeId);
    }


}