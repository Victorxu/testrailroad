package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.Passenger;
import com.tsystems.javaschool.goodoldrailroad.model.Ticket;
import com.tsystems.javaschool.goodoldrailroad.model.Voyage;

import java.util.List;

public interface TicketService {
    Ticket getTicketById(int id);
    List<Ticket> getAllTickets();
    List<Ticket> getAllTicketsByVoyage(Voyage voyage);
    List<Ticket> getAllTicketsByVoyageId(int voyageId);
    void addTicket(int voyageId, int station1Id, int station2Id, Passenger passenger);
    void deleteTicket(Ticket ticket);
    void updateTicket(Ticket ticket);
}