package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.*;
import com.tsystems.javaschool.goodoldrailroad.model.*;
import com.tsystems.javaschool.goodoldrailroad.service.api.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    private final TicketDao ticketDao;
    private final VoyageDao voyageDao;
    private final StationDao stationDao;
    private final PassengerDao passengerDao;
    private final StationOnRouteDao stationOnRouteDao;

    @Autowired
    public TicketServiceImpl(TicketDao ticketDao, VoyageDao voyageDao, StationDao stationDao, PassengerDao passengerDao, StationOnRouteDao stationOnRouteDao) {
        this.ticketDao = ticketDao;
        this.voyageDao = voyageDao;
        this.stationDao = stationDao;
        this.passengerDao = passengerDao;
        this.stationOnRouteDao = stationOnRouteDao;
    }

    public Ticket getTicketById(int id) {
        return ticketDao.getTicket(id);
    }

    public List<Ticket> getAllTickets() {
        return ticketDao.getAllTickets();
    }

    @Transactional
    public List<Ticket> getAllTicketsByVoyage(Voyage voyage) {
        if (null != voyage) {
            return ticketDao.getTicketsForVoyage(voyage);
        } else {
            //change later to message "no such route"
            // add exception
            return new ArrayList<>();
        }
    }

    @Transactional
    public List<Ticket> getAllTicketsByVoyageId(int voyageId) {
        Voyage voyage = voyageDao.getVoyage(voyageId);
        if (null != voyage) {
            return ticketDao.getTicketsForVoyage(voyage);
        } else {
            //change later to message "no such route"
            // add exception
            return new ArrayList<>();
        }
    }


    @Transactional
    public void addTicket(int voyageId, int station1Id, int station2Id, Passenger passenger) {

        // check if voyage and stations exist
        Voyage voyage = voyageDao.getVoyage(voyageId);
        Station station1 = stationDao.getStation(station1Id);
        Station station2 = stationDao.getStation(station2Id);
        if ((null == voyage) || (null == station1) || (null == station2)) {
            // add exception
            return;
        }

        // check if passenger data correct
        if ((null == passenger.getName()) || (null == passenger.getSurname()) || (null == passenger.getBirthdate())) {
            // add exception
            return;
        }
        if (("".equals(passenger.getName())) || ("".equals(passenger.getSurname()))) {
            // add exception
            return;
        }
        if ((null == passenger.getBirthdate()) || (passenger.getBirthdate().after(new Date()))) {
            // add exception
            return;
        }

        // check if stations really on this route
        Route route = voyage.getRoute();
        if ((!route.getStations().contains(station1)) || !route.getStations().contains(station2)) {
            // add exception
            return;
        }

        // check if passenger exist in database, add if not
        Passenger validPassenger = passengerDao.getPassenger(passenger);
        if (null == validPassenger) {
            passengerDao.addPassenger(passenger);
            validPassenger = passengerDao.getPassenger(passenger);
        }

        // check if any seats available
        int ticketsSold = voyage.getTickets().size();
        int trainCapacity = voyage.getTrain().getCapacity();
        if (ticketsSold >= trainCapacity) {
            //tickets sold!
            // add exception
            return;
        }

        // check if it's more than 10min before departure
        int routeId = voyage.getRoute().getId();
        long timeFromStartToStationA = stationOnRouteDao.get(station1Id, routeId).getArrivalTime().getTime();
        long startTimeFromStation1 = voyage.getStartTime().getTime() + timeFromStartToStationA;
        long now = new Date().getTime();
        if ((now + 600000) > startTimeFromStation1) {
            //too late!
            // add exception
            return;
        }

        // check if such passenger already registered
        List<Ticket> soldTickets = voyage.getTickets();
        for (Ticket t : soldTickets) {
            if (t.getPassenger().equals(validPassenger)) {
                // add exception
                return;
            }
        }

        Ticket ticket = new Ticket();

        // костыль, поменять цену на устанавливаемую (где?)
        ticket.setPrice(10);
        ticket.setPassenger(validPassenger);
        ticket.setStation1(station1);
        ticket.setStation2(station2);
        ticket.setVoyage(voyage);
        ticketDao.addTicket(ticket);
    }

    public void deleteTicket(Ticket ticket) {
        ticketDao.deleteTicket(ticket);
    }

    public void updateTicket(Ticket ticket) {
        ticketDao.updateTicket(ticket);
    }
}