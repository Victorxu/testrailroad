package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.User;

import java.util.List;

public interface UserService {
    User getUser(int id);
    User getUser(String login);
    List<User> getAllUsers();
    void addUser(User user);
    void deleteUser(User user);
    void updateUser(User user);
    void saveOrUpdateUser(User user);

}
