package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.Voyage;

import java.util.Date;
import java.util.List;

public interface VoyageService {
    Voyage getVoyage(int id);
    List<Voyage> getAllVoyages();
    void addVoyage(Voyage voyage);
    void deleteVoyage(Voyage voyage);
    void updateVoyage(Voyage voyage);
    List<Voyage> getVoyagesFromTo(int stationAId, int stationBId, Date fromTime, Date toTime);
}