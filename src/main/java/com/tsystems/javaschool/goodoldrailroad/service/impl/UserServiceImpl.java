package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.controller.OldMainController;
import com.tsystems.javaschool.goodoldrailroad.dao.api.UserDao;
import com.tsystems.javaschool.goodoldrailroad.model.User;
import com.tsystems.javaschool.goodoldrailroad.service.api.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger log = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    public User getUser(int id) {
        return userDao.getUser(id);
    }

    public User getUser(String login) {
        return userDao.getUser(login);
    }

    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    public void addUser(User user) {
        userDao.addUser(user);
    }

    public void deleteUser(User user) {
        userDao.deleteUser(user);
    }

    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    @Transactional
    public void saveOrUpdateUser(User user) {
        Integer userId = user.getId();
        log.debug("UserId during adding new user: " + userId);
        if ((null == userId) || (null == getUser(userId))) {
            userDao.addUser(user);
        } else {
            userDao.updateUser(user);
        }

    }
}