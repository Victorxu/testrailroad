package com.tsystems.javaschool.goodoldrailroad.service.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.LineDao;
import com.tsystems.javaschool.goodoldrailroad.model.Line;
import com.tsystems.javaschool.goodoldrailroad.service.api.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LineServiceImpl implements LineService {

    private final LineDao lineDao;

    @Autowired
    public LineServiceImpl(LineDao lineDao) {
        this.lineDao = lineDao;
    }

    public Line getLine(int id) {
        return lineDao.getLine(id);
    }

    public List<Line> getAllLines() {
        return lineDao.getAllLines();
    }

    public void addLine(Line line) {
        lineDao.addLine(line);
    }

    public void deleteLine(Line line) {
        lineDao.deleteLine(line);
    }

    public void updateLine(Line line) {
        lineDao.updateLine(line);
    }
}
