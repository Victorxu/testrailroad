package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.Station;
import com.tsystems.javaschool.goodoldrailroad.model.TimetableRow;
import java.util.List;

public interface StationService {
    Station getStationById(int id);
    List<Station> getAllStations();
    List<Station> getStationsByLine(int lineId);
    void addStation(Station station);
    void addStation(String name, int lineId, boolean isTransit);
    void deleteStation(Station station);
    void updateStation(Station station);
    List<TimetableRow> getScheduleForStation(int stationId);
}