package com.tsystems.javaschool.goodoldrailroad.service.api;

import com.tsystems.javaschool.goodoldrailroad.model.Train;
import java.util.List;

public interface TrainService {
    Train getTrain(int id);
    List<Train> getAllTrains();
    void addTrain(Train train);
    void addTrain(int capacity);
    void updateTrain(Train train);
}