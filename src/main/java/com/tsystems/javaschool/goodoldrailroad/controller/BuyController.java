package com.tsystems.javaschool.goodoldrailroad.controller;

import com.tsystems.javaschool.goodoldrailroad.dto.FromTimeToTimeDto;
import com.tsystems.javaschool.goodoldrailroad.dto.PassengerDto;
import com.tsystems.javaschool.goodoldrailroad.model.Passenger;
import com.tsystems.javaschool.goodoldrailroad.model.Station;
import com.tsystems.javaschool.goodoldrailroad.model.TimetableRow;
import com.tsystems.javaschool.goodoldrailroad.model.Voyage;
import com.tsystems.javaschool.goodoldrailroad.service.api.StationService;
import com.tsystems.javaschool.goodoldrailroad.service.api.TicketService;
import com.tsystems.javaschool.goodoldrailroad.service.api.VoyageService;
import com.tsystems.javaschool.goodoldrailroad.util.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class BuyController {

    @Autowired
    private StationService stationService;
    @Autowired
    private VoyageService voyageService;
    @Autowired
    private TicketService ticketService;
    @Autowired
    private Converter converter;


    @GetMapping(value = "/buy/{stationId1}")
    public String buy1Page(@PathVariable("stationId1") final int stationId1, Model model) {

        //check if correct station1
        Station station1 = stationService.getStationById(stationId1);
        int line = station1.getLine().getId();
        List<Station> stations2 = stationService.getStationsByLine(line);

        for (Station st : stations2) {
            if (st.getId() == stationId1) {
                stations2.remove(st);
                break;
            }
        }

        model.addAttribute("station1", station1);
        model.addAttribute("stations2", stations2);
        return "buy";
    }

    @GetMapping(value = "/buy/{stationId1}/{stationId2}")
    public String buy2Page(@PathVariable("stationId1") final int stationId1,
                           @PathVariable("stationId2") final int stationId2, Model model) {

        //make check if correct stations
        Station station1 = stationService.getStationById(stationId1);
        Station station2 = stationService.getStationById(stationId2);
        List<Voyage> voyages = voyageService.getVoyagesFromTo
                (stationId1, stationId2, new Date(0), new Date(new Date().getTime() + 3000000000000L));

        List<TimetableRow> timetable = stationService.getScheduleForStation(stationId1);
        List<TimetableRow> filteredTimetable = new ArrayList<>();

        //make check if null
        for (Voyage v : voyages) {
            for (TimetableRow ttu : timetable) {
                if (v.getId() == ttu.getVoyageId()) {
                    filteredTimetable.add(ttu);
                }
            }
        }

        model.addAttribute("station1", station1);
        model.addAttribute("station2", station2);
        model.addAttribute("timetable", filteredTimetable);
        model.addAttribute("fromTimeToTimeDto", new FromTimeToTimeDto());

        return "buy2";
    }

    @PostMapping(value = "/buy/{stationId1}/{stationId2}")
    public String buy2Post(@PathVariable("stationId1") final int stationId1,
                           @PathVariable("stationId2") final int stationId2,
                           @ModelAttribute("fromTimeToTimeDto") final FromTimeToTimeDto fromTimeToTimeDto,
                           Model model) {
        Station station1 = stationService.getStationById(stationId1);
        Station station2 = stationService.getStationById(stationId2);
        List<Voyage> voyages = new ArrayList<>();

        try {
            Date fromTime;
            Date toTime;
            SimpleDateFormat formatter = new SimpleDateFormat(Converter.DATE_AND_TIME_PATTERN);
            fromTime = formatter.parse(fromTimeToTimeDto.getFromTime());
            toTime = formatter.parse(fromTimeToTimeDto.getToTime());
            voyages = voyageService.getVoyagesFromTo(stationId1, stationId2, fromTime, toTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<TimetableRow> timetable = stationService.getScheduleForStation(stationId1);
        List<TimetableRow> filteredTimetable = new ArrayList<>();

        //make check if null
        for (Voyage v : voyages) {
            for (TimetableRow ttu : timetable) {
                if (v.getId() == ttu.getVoyageId()) {
                    filteredTimetable.add(ttu);
                }
            }
        }

        model.addAttribute("station1", station1);
        model.addAttribute("station2", station2);
        model.addAttribute("timetable", filteredTimetable);
        model.addAttribute("fromTimeToTimeDto", fromTimeToTimeDto);

        return "buy2";
    }


    @GetMapping(value = "/buy/{stationId1}/{stationId2}/{voyageId}")
    public String passengerPage(@PathVariable("stationId1") final int stationId1, @PathVariable("stationId2") final int stationId2,
                                @PathVariable("voyageId") final int voyageId, Model model) {
        Station station1 = stationService.getStationById(stationId1);
        Station station2 = stationService.getStationById(stationId2);
        Voyage voyage = voyageService.getVoyage(voyageId);
        List<TimetableRow> timetable = stationService.getScheduleForStation(stationId1);
        TimetableRow ttu = new TimetableRow();
        for (TimetableRow t : timetable) {
            if (t.getVoyageId() == voyageId) {
                ttu = t;
                break;
            }
        }
        PassengerDto passengerDto = new PassengerDto();

        model.addAttribute("station1", station1);
        model.addAttribute("station2", station2);
        model.addAttribute("voyage", voyage);
        model.addAttribute("ttu", ttu);
        model.addAttribute("passengerDto", passengerDto);

        return "passenger";
    }

    @PostMapping(value = "/buy/{stationId1}/{stationId2}/{voyageId}/ticket")
    public String getPassengerDetails(@ModelAttribute("passengerDto") PassengerDto passengerDto,
                                      @PathVariable("stationId1") int stationId1, @PathVariable("stationId2") int stationId2,
                                      @PathVariable("voyageId") int voyageId) {
        Passenger passenger = converter.toPassenger(passengerDto);
        ticketService.addTicket(voyageId, stationId1, stationId2, passenger);
        return "ticket";
    }
}
