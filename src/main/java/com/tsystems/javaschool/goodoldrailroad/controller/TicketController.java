package com.tsystems.javaschool.goodoldrailroad.controller;

import com.tsystems.javaschool.goodoldrailroad.model.Ticket;
import com.tsystems.javaschool.goodoldrailroad.service.api.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import java.util.List;

@Controller
public class TicketController {

    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping(value = "/voyages/{voyageId}")
    public String viewBoughtTicketsPage(@PathVariable("voyageId") int voyageId, Model model) {
        List<Ticket> tickets = ticketService.getAllTicketsByVoyageId(voyageId);
        model.addAttribute("voyageId", voyageId);
        model.addAttribute("tickets", tickets);
        return "view_bought_tickets";
    }
}
