package com.tsystems.javaschool.goodoldrailroad.controller;

import com.tsystems.javaschool.goodoldrailroad.dto.StationDto;
import com.tsystems.javaschool.goodoldrailroad.service.api.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


import javax.servlet.http.HttpSession;

@Controller
public class StationController {

    private final StationService stationService;

    @Autowired
    public StationController(StationService stationService) {
        this.stationService = stationService;
    }

    @GetMapping(value = "/stations")
    public String stationsPage(Model model, HttpSession session) {
        if ((null == session.getAttribute("sessionUser"))) {
            return "redirect:/login";
        }
        model.addAttribute("stationDto", new StationDto());
        model.addAttribute("stations", stationService.getAllStations());
        return "stations";
    }

    @PostMapping(value = "/stations")
    public String stationsPost(@ModelAttribute("stationDto") StationDto stationDto, HttpSession session) {
        if ((null == session.getAttribute("sessionUser"))) {
            return "redirect:/login";
        }
        stationService.addStation(stationDto.getName(), stationDto.getLineId(), false);
        return "redirect:/stations";
    }

}
