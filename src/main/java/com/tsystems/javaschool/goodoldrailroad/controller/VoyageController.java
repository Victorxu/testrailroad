package com.tsystems.javaschool.goodoldrailroad.controller;

import com.tsystems.javaschool.goodoldrailroad.dto.VoyageDto;
import com.tsystems.javaschool.goodoldrailroad.model.Voyage;
import com.tsystems.javaschool.goodoldrailroad.service.api.VoyageService;
import com.tsystems.javaschool.goodoldrailroad.util.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

@Controller
public class VoyageController {

    private final VoyageService voyageService;
    private final Converter converter;

    @Autowired
    public VoyageController(VoyageService voyageService, Converter converter) {
        this.voyageService = voyageService;
        this.converter = converter;
    }


    @GetMapping(value = "/voyages")
    public String voyagesPage(Model model, HttpSession session) {
        if ((null == session.getAttribute("sessionUser"))) {
            return "redirect:/login";
        }
        model.addAttribute("voyageDto", new VoyageDto());
        model.addAttribute("voyages", voyageService.getAllVoyages());
        return "voyages";
    }

    @PostMapping(value = "/voyages")
    public String voyagesPost(@ModelAttribute("voyageDto") VoyageDto voyageDto) {
        Voyage voyage = converter.toVoyage(voyageDto);
        voyageService.addVoyage(voyage);
        return "redirect:/voyages";
    }
}
