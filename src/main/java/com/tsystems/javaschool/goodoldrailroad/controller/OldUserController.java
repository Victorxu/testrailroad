package com.tsystems.javaschool.goodoldrailroad.controller;

import com.tsystems.javaschool.goodoldrailroad.dto.UserDto;
import com.tsystems.javaschool.goodoldrailroad.model.User;
import com.tsystems.javaschool.goodoldrailroad.service.api.UserService;
import com.tsystems.javaschool.goodoldrailroad.util.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


import javax.servlet.http.HttpSession;

@Controller
public class OldUserController {

    private final UserService userService;
    private final Converter converter;

    @Autowired
    public OldUserController(UserService userService, Converter converter) {
        this.userService = userService;
        this.converter = converter;
    }

    @GetMapping(value = "/admin")
    public String adminPage(Model model, HttpSession session) {
        if ((null == session.getAttribute("sessionUser"))) {
            return "redirect:/login";
        }
        return "admin";
    }

    @GetMapping(value = "/login")
    public String loginPage(Model model) {
        model.addAttribute("loginUser", new UserDto());
        return "login";
    }

    @PostMapping(value = "/login")
    public String getIn(@ModelAttribute("loginUser") UserDto userDto, HttpSession session) {
        User user=userService.getUser(userDto.getLogin());
        if (null == user) {
            session.setAttribute("noUser", userDto.getLogin());
            return "redirect:/nouserexception";
        }
        session.setAttribute("sessionUser", converter.toUserDto(user));
        return "redirect:/admin";
    }

    @GetMapping(value = "/logout")
    public String logout(HttpSession session) {
        session.removeAttribute("sessionUser");
        return "redirect:/admin";
    }

    @GetMapping(value = "/register")
    public String registerPage(Model model, HttpSession session) {
        if ((null == session.getAttribute("sessionUser"))) {
            return "redirect:/login";
        }
        model.addAttribute("newUser", new User());
        return "register";
    }

    @PostMapping(value = "/register")
    public String registerPost(@ModelAttribute("newUser") User user, HttpSession session) {
        if (null != userService.getUser(user.getLogin())) {
            session.setAttribute("errorMessage", "User already exists");
            return "error1";
        }
        user.setRole("user");
        userService.addUser(user);
        return "redirect:/admin";
    }

}
