package com.tsystems.javaschool.goodoldrailroad.controller;

import com.tsystems.javaschool.goodoldrailroad.service.api.StationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class OldMainController {
    private static final Logger log = Logger.getLogger(OldMainController.class);

    @Autowired
    private StationService stationService;

//    @GetMapping(value = "/")
//    public String homePage(Model model) {
//        model.addAttribute("station", new Station());
//        model.addAttribute("stations", stationService.getAllStations());
//        return "home";
//    }
}

