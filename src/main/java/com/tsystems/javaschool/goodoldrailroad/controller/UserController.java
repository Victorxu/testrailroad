package com.tsystems.javaschool.goodoldrailroad.controller;


import com.tsystems.javaschool.goodoldrailroad.model.User;
import com.tsystems.javaschool.goodoldrailroad.service.api.UserService;
import com.tsystems.javaschool.goodoldrailroad.util.validator.UserFormValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class UserController {

    private static final Logger log = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    UserFormValidator userFormValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(userFormValidator);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        log.debug("index()");
        return "redirect:/users";
    }

    // list page
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String showAllUsers(Model model) {

        log.debug("showAllUsers()");
        model.addAttribute("users", userService.getAllUsers());
        return "users/list";

    }

    // save or update user
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String saveOrUpdateUser(@ModelAttribute("userForm") @Validated User user,
                                   BindingResult result, Model model, final RedirectAttributes redirectAttributes) {

        log.debug("saveOrUpdateUser() : {}" + user);

        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "users/userform";
        } else {

            redirectAttributes.addFlashAttribute("css", "success");
            if (user.isNew()) {
                redirectAttributes.addFlashAttribute("msg", "User added successfully!");
            } else {
                redirectAttributes.addFlashAttribute("msg", "User updated successfully!");
            }

            userService.saveOrUpdateUser(user);

            return "redirect:/users/" + user.getId();
        }

    }

    // show add user form
    @RequestMapping(value = "/users/add", method = RequestMethod.GET)
    public String showAddUserForm(Model model) {

        log.debug("showAddUserForm()");

        User user = new User();

        // set default value
        user.setLogin("New User");
        user.setRole("staff");

        model.addAttribute("userForm", user);

        populateDefaultModel(model);

        return "users/userform";

    }

    // show update form
    @RequestMapping(value = "/users/{id}/update", method = RequestMethod.GET)
    public String showUpdateUserForm(@PathVariable("id") int id, Model model) {

        log.debug("showUpdateUserForm() : {}" + id);

        User user = userService.getUser(id);
        model.addAttribute("userForm", user);

        populateDefaultModel(model);

        return "users/userform";

    }

    // delete user
    @RequestMapping(value = "/users/{id}/delete", method = RequestMethod.POST)
    public String deleteUser(@PathVariable("id") int id, final RedirectAttributes redirectAttributes) {

        // check if user exists
        User user = userService.getUser(id);

        if (null == user) {
            redirectAttributes.addFlashAttribute("css", "danger");
            redirectAttributes.addFlashAttribute("msg", "User is not deleted!");

            return "redirect:/users";
        }

        log.debug("deleteUser() : {}" + id);

        userService.deleteUser(user);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "User is deleted!");

        return "redirect:/users";

    }

    // show user
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String showUser(@PathVariable("id") int id, Model model) {

        log.debug("showUser() id: {}" + id);

        User user = userService.getUser(id);
        if (null == user) {
            model.addAttribute("css", "danger");
            model.addAttribute("msg", "User not found");
        }
        model.addAttribute("user", user);

        return "users/show";

    }

    private void populateDefaultModel(Model model) {
        List<String> roleList = new ArrayList<String>();
        roleList.add("admin");
        roleList.add("staff");
        roleList.add("user");
        model.addAttribute("roleList", roleList);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ModelAndView handleEmptyData(HttpServletRequest req, Exception ex) {

        log.debug("handleEmptyData()");
        log.error("Request: {}, error " + req.getRequestURL(), ex);

        ModelAndView model = new ModelAndView();
        model.setViewName("user/show");
        model.addObject("msg", "user not found");

        return model;

    }

}