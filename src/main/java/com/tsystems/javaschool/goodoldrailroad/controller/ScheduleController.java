package com.tsystems.javaschool.goodoldrailroad.controller;

import com.tsystems.javaschool.goodoldrailroad.model.TimetableRow;
import com.tsystems.javaschool.goodoldrailroad.service.api.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import java.util.List;

@Controller
public class ScheduleController {

    private final StationService stationService;

    @Autowired
    public ScheduleController(StationService stationService) {
        this.stationService = stationService;
    }

    @GetMapping(value = "/schedule/{stationId}")
    public String schedulePage(@PathVariable("stationId") int stationId, Model model) {
        List<TimetableRow> timetable = stationService.getScheduleForStation(stationId);
        model.addAttribute("timetable", timetable);
        model.addAttribute("stationId", stationId);
        return "schedule";
    }
}
