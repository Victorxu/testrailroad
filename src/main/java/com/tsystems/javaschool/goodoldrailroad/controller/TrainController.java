package com.tsystems.javaschool.goodoldrailroad.controller;

import com.tsystems.javaschool.goodoldrailroad.model.Train;
import com.tsystems.javaschool.goodoldrailroad.service.api.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

@Controller
public class TrainController {

    private final TrainService trainService;

    @Autowired
    public TrainController(TrainService trainService) {
        this.trainService = trainService;
    }

    @GetMapping(value = "/trains")
    public String trainsPage(Model model, HttpSession session) {
        if ((null == session.getAttribute("sessionUser"))) {
            return "redirect:/login";
        }
        model.addAttribute("train", new Train());
        model.addAttribute("trains", trainService.getAllTrains());
        return "trains";
    }

    @PostMapping(value = "/trains")
    public String trainsPost(@ModelAttribute("train") Train train, HttpSession session) {
        if ((null == session.getAttribute("sessionUser"))) {
            return "redirect:/login";
        }
        trainService.addTrain(train);
        return "redirect:/trains";
    }


}
