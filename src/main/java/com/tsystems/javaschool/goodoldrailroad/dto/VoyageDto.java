package com.tsystems.javaschool.goodoldrailroad.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoyageDto {
    private String startTime;
    private String status;
    private String delay;
    private int trainId;
    private int routeId;
}