package com.tsystems.javaschool.goodoldrailroad.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable{
    private Integer id;
    private String login;
    private String password;
    private String confirmPassword;
    private String role;
}
