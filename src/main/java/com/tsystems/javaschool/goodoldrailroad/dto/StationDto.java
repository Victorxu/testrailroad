package com.tsystems.javaschool.goodoldrailroad.dto;

import lombok.Data;

@Data
public class StationDto {
    private String name;
    private boolean isTransit;
    private int lineId;
}
