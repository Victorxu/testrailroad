package com.tsystems.javaschool.goodoldrailroad.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassengerDto {
    private String name;
    private String surname;
    private String birthdate;
}
