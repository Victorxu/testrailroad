package com.tsystems.javaschool.goodoldrailroad.dto;

import lombok.Data;

@Data
public class FromTimeToTimeDto {
    private String fromTime;
    private String toTime;
}
