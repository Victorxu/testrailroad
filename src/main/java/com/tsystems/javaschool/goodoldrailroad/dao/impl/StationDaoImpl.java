package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.StationDao;
import com.tsystems.javaschool.goodoldrailroad.model.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;


import org.hibernate.SessionFactory;

@Repository
public class StationDaoImpl implements StationDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<Station> getAllStations() {
        return sessionFactory.getCurrentSession().createQuery("FROM Station ORDER BY line.id").list();
    }

    @Transactional
    public List<Station> getStationsOnLine(int lineId){
        return sessionFactory.getCurrentSession()
                .createQuery("FROM Station WHERE line.id=:line_id")
                .setParameter("line_id", lineId)
                .list();
    }

    @Transactional
    public Station getStation(int id){
        return sessionFactory.getCurrentSession().get(Station.class, id);
    }

    @Transactional
    public void addStation(Station station){
        sessionFactory.getCurrentSession().persist(station);
    }

    @Transactional
    public void updateStation(Station station){
        sessionFactory.getCurrentSession().merge(station);
    }

    @Transactional
    public void deleteStation(Station station){
        sessionFactory.getCurrentSession().delete(station);
    }

}
