package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.PassengerDao;
import com.tsystems.javaschool.goodoldrailroad.model.Passenger;
import org.hibernate.SessionFactory;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class PassengerDaoImpl implements PassengerDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<Passenger> getAllPassengers() {
        return sessionFactory.getCurrentSession().createQuery("FROM Passenger").list();
    }

    @Transactional
    public Passenger getPassenger(int id) {
        return sessionFactory.getCurrentSession().get(Passenger.class, id);
    }

    @Transactional
    public Passenger getPassenger(Passenger passenger) {
        Passenger p = null;
        try {
            p = (Passenger) sessionFactory.getCurrentSession()
                    .createQuery("FROM Passenger WHERE name=:name AND surname=:surname AND birthdate=:birthdate")
                    .setParameter("name", passenger.getName())
                    .setParameter("surname", passenger.getSurname())
                    .setParameter("birthdate", passenger.getBirthdate())
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (DataException dae) {
            dae.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;
    }

    @Transactional
    public void addPassenger(Passenger passenger) {
        sessionFactory.getCurrentSession().persist(passenger);
    }

    @Transactional
    public void updatePassenger(Passenger passenger) {
        sessionFactory.getCurrentSession().merge(passenger);
    }

    @Transactional
    public void deletePassenger(Passenger passenger) {
        sessionFactory.getCurrentSession().delete(passenger);
    }
}
