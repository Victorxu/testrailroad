package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.Train;
import java.util.List;

public interface TrainDao {
    Train getTrain(int id);
    List<Train> getAllTrains();
    void addTrain(Train train);
    void updateTrain(Train train);
}