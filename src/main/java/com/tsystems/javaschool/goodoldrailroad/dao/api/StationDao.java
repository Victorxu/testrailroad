package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.Station;

import java.util.List;

public interface StationDao {
    Station getStation(int id);
    List<Station> getAllStations();
    List<Station> getStationsOnLine(int lineId);
    void addStation(Station station);
    void updateStation(Station station);
    void deleteStation(Station station);
}