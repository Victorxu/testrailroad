package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.StationOnRouteDao;
import com.tsystems.javaschool.goodoldrailroad.model.Route;
import com.tsystems.javaschool.goodoldrailroad.model.Station;
import com.tsystems.javaschool.goodoldrailroad.model.StationOnRoute;
import org.hibernate.SessionFactory;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.sql.Time;
import java.util.List;

//check if needed at all
//check all methods!
@Repository
public class StationOnRouteDaoImpl implements StationOnRouteDao {

    @Autowired
    private SessionFactory sessionFactory;

    //check!
    @Transactional
    public List<Station> getStationsOnRoute(int routeId) {
        return sessionFactory.getCurrentSession()
                .createQuery("SELECT Station FROM StationOnRoute WHERE route.id=:routeId")
                .setParameter("routeId", routeId)
                .list();
    }

    //check!
    @Transactional
    public List<Route> getRoutesForStation(int stationId) {
        List<Route> routes;
        routes = sessionFactory.getCurrentSession()
                .createQuery("SELECT Route FROM StationOnRoute WHERE station.id=:stationId")
                .setParameter("stationId", stationId)
                .list();
        return routes;
    }

    @Transactional
    public List<StationOnRoute> getAll() {
        return sessionFactory.getCurrentSession().createQuery("FROM StationOnRoute").list();
    }

    //probably move to service layer
    @Transactional
    public void addList(List<Station> stations, Route route) {
        int i = 1;
        for (Station st : stations) {
            this.add(st, i++, route);
        }
    }

    //probably move to service layer
    @Transactional
    public void addList(List<StationOnRoute> stationsOnRoute) {
        for (StationOnRoute st : stationsOnRoute) {
            this.add(st);
        }
    }

    @Transactional
    public void add(StationOnRoute stationOnRoute) {
        sessionFactory.getCurrentSession().persist(stationOnRoute);
    }

    //move all logic to service layer
    @Transactional
    public void add(Station station, int stationIndexOnRoute, Route route) {

        StationOnRoute stationOnRoute = new StationOnRoute();

        //change later - to manually entered value
        stationOnRoute.setArrivalTime(new Time(300000L * (stationIndexOnRoute - 1)));

        //change later - to manually entered value
        stationOnRoute.setStayTime(new Time(60000L));
        stationOnRoute.setStationIndexInRoute(stationIndexOnRoute);
        stationOnRoute.setRoute(route);
        stationOnRoute.setStation(station);

        this.add(stationOnRoute);
    }

    @Transactional
    public void update(StationOnRoute stationOnRoute) {
        sessionFactory.getCurrentSession().merge(stationOnRoute);
    }

    @Transactional
    public void delete(StationOnRoute stationOnRoute) {
        sessionFactory.getCurrentSession().delete(stationOnRoute);
    }

    @Transactional
    public List<StationOnRoute> getAllOnRoute(int routeId) {
        return sessionFactory.getCurrentSession()
                .createQuery("FROM StationOnRoute WHERE route.id=:routeId")
                .setParameter("routeId", routeId)
                .list();
    }

    @Transactional
    public StationOnRoute get(int stationId, int routeId) {
        StationOnRoute sor = null;
        try {
            sor = (StationOnRoute) sessionFactory.getCurrentSession()
                    .createQuery("FROM StationOnRoute " +
                            "WHERE route.id=:routeId AND station.id=:stationId")
                    .setParameter("routeId", routeId)
                    .setParameter("stationId", stationId)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (DataException dae) {
            dae.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sor;
    }
}
