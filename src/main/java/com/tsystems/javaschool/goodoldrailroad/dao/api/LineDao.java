package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.Line;
import java.util.List;

public interface LineDao {
    Line getLine(int id);
    List<Line> getAllLines();
    void addLine(Line line);
    void updateLine(Line line);
    void deleteLine(Line line);
}

