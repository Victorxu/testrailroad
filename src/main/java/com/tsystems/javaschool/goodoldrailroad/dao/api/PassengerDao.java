package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.Passenger;
import java.util.List;

public interface PassengerDao {
    Passenger getPassenger(int id);
    List<Passenger> getAllPassengers();
    Passenger getPassenger(Passenger passenger);
    void addPassenger(Passenger passenger);
    void updatePassenger(Passenger passenger);
    void deletePassenger(Passenger passenger);
}
