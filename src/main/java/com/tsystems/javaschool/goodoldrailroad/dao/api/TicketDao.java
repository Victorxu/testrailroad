package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.Ticket;
import com.tsystems.javaschool.goodoldrailroad.model.Voyage;

import java.util.List;

public interface TicketDao {
    Ticket getTicket(int id);
    List<Ticket> getAllTickets();
    List<Ticket> getTicketsForVoyage(Voyage voyage);
    void addTicket(Ticket ticket);
    void updateTicket(Ticket ticket);
    void deleteTicket(Ticket ticket);
}