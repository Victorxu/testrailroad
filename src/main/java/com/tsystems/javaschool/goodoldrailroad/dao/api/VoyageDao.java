package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.Voyage;
import java.util.List;

public interface VoyageDao {
    Voyage getVoyage(int id);
    List<Voyage> getAllVoyages();
    void addVoyage(Voyage voyage);
    void updateVoyage(Voyage voyage);
    void deleteVoyage(Voyage voyage);
}