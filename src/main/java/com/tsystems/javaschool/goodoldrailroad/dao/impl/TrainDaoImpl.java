package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.TrainDao;
import com.tsystems.javaschool.goodoldrailroad.model.Train;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TrainDaoImpl implements TrainDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<Train> getAllTrains() {
        return sessionFactory.getCurrentSession().createQuery("FROM Train").list();
    }

    @Transactional
    public Train getTrain(int id){
        return sessionFactory.getCurrentSession().get(Train.class, id);
    }

    @Transactional
    public void addTrain(Train train){
        sessionFactory.getCurrentSession().persist(train);
    }

    @Transactional
    public void updateTrain(Train train){
        sessionFactory.getCurrentSession().merge(train);
    }
}
