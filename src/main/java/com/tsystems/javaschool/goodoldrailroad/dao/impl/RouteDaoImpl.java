package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.RouteDao;
import com.tsystems.javaschool.goodoldrailroad.model.Route;
import org.hibernate.SessionFactory;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class RouteDaoImpl implements RouteDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<Route> getAllRoutes() {
        return sessionFactory.getCurrentSession().createQuery("FROM Route").list();
    }

    @Transactional
    public Route getRoute(int id) {
        return sessionFactory.getCurrentSession().get(Route.class, id);
    }

    @Transactional
    public Route getRoute(String name) {
        Route route = null;
        try {
            route = (Route) sessionFactory.getCurrentSession()
                    .createQuery("FROM Route WHERE name=:name")
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (DataException dae) {
            dae.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return route;
    }

    @Transactional
    public void addRoute(Route route) {
        sessionFactory.getCurrentSession().persist(route);
    }

    @Transactional
    public void updateRoute(Route route) {
        sessionFactory.getCurrentSession().merge(route);
    }

    @Transactional
    public void deleteRoute(Route route) {
        sessionFactory.getCurrentSession().delete(route);
    }
}
