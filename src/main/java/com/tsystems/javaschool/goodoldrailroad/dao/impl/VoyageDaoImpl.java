package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.VoyageDao;
import com.tsystems.javaschool.goodoldrailroad.model.Voyage;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class VoyageDaoImpl implements VoyageDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<Voyage> getAllVoyages() {
        return sessionFactory.getCurrentSession().createQuery("FROM Voyage").list();
    }

    @Transactional
    public Voyage getVoyage(int id) {
        return sessionFactory.getCurrentSession().get(Voyage.class, id);
    }

    @Transactional
    public void addVoyage(Voyage voyage) {
        sessionFactory.getCurrentSession().persist(voyage);
    }

    @Transactional
    public void updateVoyage(Voyage voyage) {
        sessionFactory.getCurrentSession().merge(voyage);
    }

    @Transactional
    public void deleteVoyage(Voyage voyage) {
        sessionFactory.getCurrentSession().delete(voyage);
    }
}
