package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.LineDao;
import com.tsystems.javaschool.goodoldrailroad.model.Line;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class LineDaoImpl implements LineDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<Line> getAllLines() {
        return sessionFactory.getCurrentSession().createQuery("FROM Line").list();
    }

    @Transactional
    public Line getLine(int id) {
        return sessionFactory.getCurrentSession().get(Line.class, id);
    }

    @Transactional
    public void addLine(Line line) {
        sessionFactory.getCurrentSession().persist(line);
    }

    @Transactional
    public void updateLine(Line line) {
        sessionFactory.getCurrentSession().merge(line);
    }

    @Transactional
    public void deleteLine(Line line) {
        sessionFactory.getCurrentSession().delete(line);
    }
}
