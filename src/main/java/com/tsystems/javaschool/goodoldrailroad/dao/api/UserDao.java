package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.User;

import java.util.List;

public interface UserDao {
    User getUser(int id);
    User getUser(String login);
    List<User> getAllUsers();
    void addUser(User user);
    void updateUser(User user);
    void deleteUser(User user);
}