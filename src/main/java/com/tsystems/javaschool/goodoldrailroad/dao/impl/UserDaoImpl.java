package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.UserDao;
import com.tsystems.javaschool.goodoldrailroad.model.User;
import org.hibernate.SessionFactory;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<User> getAllUsers() {
        return sessionFactory.getCurrentSession().createQuery("FROM User").list();
    }

    @Transactional
    public User getUser(int id) {
        return sessionFactory.getCurrentSession().get(User.class, id);
    }

    @Transactional
    public User getUser(String login) {
        User user = null;
        try {
            user = (User) sessionFactory.getCurrentSession()
                    .createQuery("FROM User WHERE login=:login")
                    .setParameter("login", login)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
        } catch (DataException dae) {
            dae.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    @Transactional
    public void addUser(User user) {
        sessionFactory.getCurrentSession().persist(user);
    }

    @Transactional
    public void updateUser(User user) {
        sessionFactory.getCurrentSession().merge(user);
    }

    @Transactional
    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }
}
