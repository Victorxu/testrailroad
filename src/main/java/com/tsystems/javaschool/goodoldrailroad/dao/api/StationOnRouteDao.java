package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.Route;
import com.tsystems.javaschool.goodoldrailroad.model.Station;
import com.tsystems.javaschool.goodoldrailroad.model.StationOnRoute;
import java.util.List;

//check if needed at all
public interface StationOnRouteDao {
    List<Station> getStationsOnRoute(int routeId);

    List<Route> getRoutesForStation(int stationId);

    void addList(List<Station> stations, Route route);
    void addList(List<StationOnRoute> stationsOnRoute);

    void add(Station station, int stationIndexOnRoute, Route route);
    void add(StationOnRoute stationOnRoute);

    List<StationOnRoute> getAll();
    List<StationOnRoute> getAllOnRoute(int routeId);

    void update(StationOnRoute stationOnRoute);
    void delete(StationOnRoute stationOnRoute);

    StationOnRoute get(int stationId, int routeId);
}