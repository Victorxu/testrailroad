package com.tsystems.javaschool.goodoldrailroad.dao.api;

import com.tsystems.javaschool.goodoldrailroad.model.Route;
import java.util.List;

public interface RouteDao {
    Route getRoute(int id);
    Route getRoute(String name);
    List<Route> getAllRoutes();
    void addRoute(Route route);
    void updateRoute(Route route);
    void deleteRoute(Route route);
}