package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.dao.api.TicketDao;
import com.tsystems.javaschool.goodoldrailroad.model.Ticket;
import com.tsystems.javaschool.goodoldrailroad.model.Voyage;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TicketDaoImpl implements TicketDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public List<Ticket> getAllTickets() {
        return sessionFactory.getCurrentSession().createQuery("FROM Ticket").list();
    }

    @Transactional
    public List<Ticket> getTicketsForVoyage(Voyage voyage) {
        return voyage.getTickets();
   }

    @Transactional
    public Ticket getTicket(int id){
        return sessionFactory.getCurrentSession().get(Ticket.class, id);
    }

    @Transactional
    public void addTicket(Ticket ticket){
        sessionFactory.getCurrentSession().persist(ticket);
    }

    @Transactional
    public void updateTicket(Ticket ticket){
        sessionFactory.getCurrentSession().merge(ticket);
    }

    @Transactional
    public void deleteTicket(Ticket ticket){
        sessionFactory.getCurrentSession().delete(ticket);
    }
}
