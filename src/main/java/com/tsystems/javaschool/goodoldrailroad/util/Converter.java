package com.tsystems.javaschool.goodoldrailroad.util;

import com.tsystems.javaschool.goodoldrailroad.dto.PassengerDto;
import com.tsystems.javaschool.goodoldrailroad.dto.UserDto;
import com.tsystems.javaschool.goodoldrailroad.dto.VoyageDto;
import com.tsystems.javaschool.goodoldrailroad.model.*;
import com.tsystems.javaschool.goodoldrailroad.service.api.RouteService;
import com.tsystems.javaschool.goodoldrailroad.service.api.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Converter {

    public static final String DATE_AND_TIME_PATTERN = "dd/MM/yyyy HH:mm";

    public static final String TIME_PATTERN = "HH:mm";

    public static final String DATE_PATTERN = "dd/MM/yyyy";

    @Autowired
    RouteService routeService;

    @Autowired
    TrainService trainService;

    public Voyage toVoyage(VoyageDto voyageDto) {

        // resolving Train
        Train train = trainService.getTrain(voyageDto.getTrainId());

        // resolving Route
        Route route = routeService.getRoute(voyageDto.getRouteId());

        // resolving StartTime
        Date startTime = new Date();
        try {
            startTime = toDateAndTime(voyageDto.getStartTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //setting Train, Route and StartTime
        Voyage voyage = new Voyage(route, train, startTime);

        // setting Status
        voyage.setStatus(voyageDto.getStatus());

        // setting Delay
        try {
            voyage.setDelay(toTime(voyageDto.getDelay()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return voyage;
    }

    public VoyageDto toVoyageDto(Voyage voyage) {
        String status = voyage.getStatus();
        int trainId = voyage.getTrain().getId();
        int routeId = voyage.getRoute().getId();
        String startTime = toString(voyage.getStartTime(), DATE_AND_TIME_PATTERN);
        String delay = toString(voyage.getDelay());

        return new VoyageDto(startTime, status, delay, trainId, routeId);
    }

    public String toString(Date date, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(date);
    }

    public String toString(Time time) {
        SimpleDateFormat formatter = new SimpleDateFormat(TIME_PATTERN);
        return formatter.format(time);
    }

    public Date toDateAndTime(String dateString) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_AND_TIME_PATTERN);
        return formatter.parse(dateString);
    }

    public Date toDate(String dateString) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
        return formatter.parse(dateString);
    }

    public Time toTime(String timeString) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(TIME_PATTERN);
        return new Time(formatter.parse(timeString).getTime());
    }

    public Passenger toPassenger(PassengerDto passengerDto) {
        Passenger passenger = new Passenger();

        passenger.setName(passengerDto.getName());
        passenger.setSurname(passengerDto.getSurname());
        try {
            passenger.setBirthdate(toDate(passengerDto.getBirthdate()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return passenger;
    }

    public PassengerDto toPassengerDto(Passenger passenger) {
        String name = passenger.getName();
        String surname = passenger.getSurname();
        String birthdate = toString(passenger.getBirthdate(), DATE_PATTERN);

        return new PassengerDto(name, surname, birthdate);
    }

    public UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setConfirmPassword(user.getConfirmPassword());
        userDto.setRole(user.getRole());
        return userDto;
    }

}
