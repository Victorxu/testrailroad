package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Time;
import java.util.List;

@Data
@Entity
@Table(name = "route")
public class Route {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    //    @Temporal(TemporalType.TIME)          //- exception on @Temporal ???
    private Time duration;

    //check if needed at all
    //, fetch = FetchType.EAGER
    @OneToMany(mappedBy = "route")
    @OrderBy("stationIndexInRoute asc")
    private List<StationOnRoute> stationsOnRoute;

    @OneToMany(mappedBy = "route", fetch = FetchType.EAGER)
    @OrderBy("startTime asc")
    private List<Voyage> voyages;

    @ManyToMany(fetch = FetchType.EAGER)
    //@LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "station_on_route",
            joinColumns = @JoinColumn(name = "route_id"),
            inverseJoinColumns = @JoinColumn(name = "station_id"))
    private List<Station> stations;

    public String toString() {
        return "Route{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", duration=" + duration +
                '}';
    }

}
