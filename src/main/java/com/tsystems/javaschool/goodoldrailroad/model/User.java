package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user", schema = "public")
public class User implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Basic
    @Column(name = "login", nullable = false, length = -1)
    private String login;

    @Basic
    @Column(name = "password", nullable = false, length = -1)
    private String password;

    /**
     * Non persistable, for equality-check in view layer.
     */
    private String confirmPassword;

    @Basic
    @Column(name = "role", nullable = false, length = -1)
    private String role;

    public boolean isNew() {
        return (null == this.id);
    }

    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
