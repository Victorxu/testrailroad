package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import java.util.List;

@Data
public class Timetable {
    List<TimetableRow> rowList;
}
