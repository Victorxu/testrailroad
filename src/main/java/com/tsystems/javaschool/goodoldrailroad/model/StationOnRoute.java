package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Time;

//@Table(name = "station_on_route", schema = "public", catalog = "db_goodrailroad")
@Data
@Entity
@Table(name = "station_on_route", schema = "public", catalog = "db_goodrailroad")
public class StationOnRoute {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "station_index_in_route", nullable = false)
    private int stationIndexInRoute;

    @Column(name = "arrival_time", nullable = false)
//    @Temporal(TemporalType.TIME)                       //- exception on @Temporal ???
    private Time arrivalTime;

    @Column(name = "stay_time", nullable = false)
//    @Temporal(TemporalType.TIME)                       //- exception on @Temporal ???
    private Time stayTime;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "route_id", referencedColumnName = "id", nullable = false)
    private Route route;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "station_id", referencedColumnName = "id", nullable = false)
    private Station station;

    public String toString() {
        return "StationOnRoute{" +
                "id=" + id +
                ", stationIndexInRoute=" + stationIndexInRoute +
                ", arrivalTime=" + arrivalTime +
                ", stayTime=" + stayTime +
                ", routeByRouteId=" + route +
                ", stationByStationId=" + station +
                '}';
    }

}
