package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
//@EqualsAndHashCode(exclude = {"stations"})        // change names if other code refactored
@Entity
@Table(name = "line")
public class Line {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    // remove OrderBy later, when sorting in view-layer is done
    @OneToMany(mappedBy = "line", fetch = FetchType.EAGER)
    @OrderBy("id desc")
    private List<Station> stations;

    public String toString() {
        return "Line{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
