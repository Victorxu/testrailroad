package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "train")
public class Train {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int capacity;

    @OneToMany(mappedBy = "train", fetch = FetchType.EAGER)
    @OrderBy("startTime asc")
    private List<Voyage> voyages;

    public String toString() {
        return "Train{" +
                "id=" + id +
                ", capacity=" + capacity +
                '}';
    }
}
