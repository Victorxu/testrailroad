package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private int price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "passenger_id", referencedColumnName = "id", nullable = false)
    private Passenger passenger;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "station1_id", referencedColumnName = "id", nullable = false)
    private Station station1;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "station2_id", referencedColumnName = "id", nullable = false)
    private Station station2;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "voyage_id", referencedColumnName = "id", nullable = false)
    private Voyage voyage;

    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", price=" + price +
                ", passengerByPassengerId=" + passenger +
                ", stationByStation1Id=" + station1 +
                ", stationByStation2Id=" + station2 +
                ", voyageByVoyageId=" + voyage +
                '}';
    }

}
