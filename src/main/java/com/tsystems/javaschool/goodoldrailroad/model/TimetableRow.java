package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import java.util.Date;

@Data
public class TimetableRow {

    private String routeName;

    private int voyageId;

    private Date departureTime;
}
