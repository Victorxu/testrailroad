package com.tsystems.javaschool.goodoldrailroad.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "station")
public class Station {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(name = "is_transit", nullable = false)
    private boolean isTransit;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "line_id", referencedColumnName = "id", nullable = false)
    private Line line;

    @OneToMany(mappedBy = "station1", fetch = FetchType.EAGER)
    private List<Ticket> ticketsFrom;

    @OneToMany(mappedBy = "station2", fetch = FetchType.EAGER)
    private List<Ticket> ticketsTo;

    // , cascade = CascadeType.ALL ??
    @ManyToMany(mappedBy = "stations")
    private List<Route> routes;

    public String toString() {
        return "Station{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isTransit=" + isTransit +
                ", lineByLineId=" + line +
                '}';
    }

}
