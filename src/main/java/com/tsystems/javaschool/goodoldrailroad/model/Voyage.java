package com.tsystems.javaschool.goodoldrailroad.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "voyage")
public class Voyage {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "start_time", nullable = false)
    //- exception on @Temporal ???
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    @Column(nullable = false)
    //не срабатывает почему-то, пришлось костыль в service ставить
    @ColumnDefault("'On schedule'")
    private String status;

    @Column
    //- exception on @Temporal ???
//    @Temporal(TemporalType.TIME)
    private Time delay;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "train_id", referencedColumnName = "id", nullable = false)
    private Train train;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "route_id", referencedColumnName = "id", nullable = false)
    private Route route;

    @OneToMany(mappedBy = "voyage", fetch = FetchType.EAGER)
    @OrderBy("station1.id asc, station2.id asc")
    private List<Ticket> tickets;

    public String toString() {
        return "Voyage{" +
                "id=" + id +
                ", startTime=" + startTime +
                ", status='" + status + '\'' +
                ", delay=" + delay +
                ", trainByTrainId=" + train +
                ", routeByRouteId=" + route +
                '}';
    }

    public Voyage(Route route, Train train, Date startTime) {
        this.route = route;
        this.train = train;
        this.startTime = startTime;
        this.status = "On schedule";
    }
}
