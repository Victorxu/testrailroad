<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>Trains</h2>

<table class="sortable">
    <tr>
        <td>Train id: </td>
        <td>Train capacity: </td>
    </tr>
    <br/>
    <c:forEach items="${trains}" var="train">
        <tr>
            <td> ${train.id}</td>
            <td> ${train.capacity}</td>
            <%--<td>--%>
                <%--<button onclick="location.href = '/home/delete/${train.id}'">Delete</button>--%>
            <%--</td>--%>
        </tr>
    </c:forEach>
</table>
<br/>
<c:url var="trains" value="/trains"/>
<form:form action="${trains}" modelAttribute="train">
    <div class="row">
        <div id="form-group-email" class="form-group col-lg-4 col-lg-offset-4">
            <tr>Train Capacity: </tr>
            <form:label path="capacity"/>
            <form:input path="capacity" minlength="1" required="required" placeholder="capacity"/>
            <button type="submit" class="btn btn-success">Add train</button>
        </div>
</form:form>


</body>
</html>
