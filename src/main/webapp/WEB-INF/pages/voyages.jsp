<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>Voyages</h2>

<table class="sortable">
    <tr>
        <td>Voyage id: </td>
        <td>Voyage start time: </td>
        <td>Voyage train id: </td>
        <td>Voyage train capacity: </td>
        <td>Voyage route id: </td>
        <td>Voyage route name: </td>
    </tr>
    <br/>
    <c:forEach items="${voyages}" var="voyage">
        <tr>
            <td> ${voyage.id} </td>
            <td> ${voyage.startTime.toString()}</td>
            <td> ${voyage.train.id}</td>
            <td> ${voyage.train.capacity}</td>
            <td> ${voyage.route.id}</td>
            <td> ${voyage.route.name}</td>
            <td>
                <button onclick="location.href = '/voyages/${voyage.id}'">View all tickets</button>
            </td>
        </tr>
    </c:forEach>
</table>


<c:url var="voyages" value="/voyages"/>
<form:form action="${voyages}" modelAttribute="voyageDto">

    <div class="row">
        <div id="form-wqeqwfl" class="form-group col-lg-4 col-lg-offset-4">
            <tr>trainId</tr>
            <form:label path="trainId"/>
            <form:input path="trainId" minlength="1" required="required"
                        placeholder="trainId"/>
        </div>
    </div>
    <div class="row">
        <div id="formqweq" class="form-group col-lg-4 col-lg-offset-4">
            <tr>routeId</tr>
            <form:label path="routeId"/>
            <form:input path="routeId" minlength="1" required="required"
                        placeholder="routeId"/>
        </div>
    </div>
    <div class="row">
        <div id="cfnfxbn" class="form-group col-lg-4 col-lg-offset-4">
            <tr>startTime</tr>
            <form:label path="startTime"/>
            <form:input path="startTime" minlength="8" required="required"
                        placeholder="startTime"/>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-lg-4 col-lg-offset-4">
            <button type="submit" class="btn btn-success">Add voyage</button>
        </div>
    </div>
</form:form>


</body>
</html>
