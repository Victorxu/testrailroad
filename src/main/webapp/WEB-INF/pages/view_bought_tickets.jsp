<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>/Good Old Railroad Inc./</h2>
<table class="sortable">
    <tr>
        Voyage Id: ${voyageId}
        <br/>
    </tr>
    <tr>
        <td>Passenger name:</td>
        <td>Passenger surname:</td>
        <td>Passenger birth date:</td>
        <td>Ticket id:</td>
        <td>Ticket price:</td>
    </tr>
    <br/>
    <c:forEach items="${tickets}" var="ticket">
        <tr>
            <td>${ticket.passenger.name}</td>
            <td>${ticket.passenger.surname}</td>
            <td>${ticket.passenger.birthdate.toString()}</td>
            <td>${ticket.id}</td>
            <td>${ticket.price}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>