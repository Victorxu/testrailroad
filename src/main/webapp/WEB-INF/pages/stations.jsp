<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>Stations</h2>



<table class="sortable">
    <tr>
        <td>Station id: </td>
        <td>Station name: </td>
        <td>Line: </td>
    </tr>
    <br/>
    <c:forEach items="${stations}" var="station">
        <tr>
            <td> ${station.id}</td>
            <td> ${station.name}</td>
            <td> ${station.line.id}</td>
        </tr>
    </c:forEach>
</table>

<br/>
<c:url var="stations" value="/stations"/>
<form:form action="${stations}" modelAttribute="stationDto">

    <div class="row">
        <div id="form-wqeqwfl" class="form-group col-lg-4 col-lg-offset-4">
            <tr>Line Id</tr>
            <form:label path="lineId"/>
            <form:input path="lineId" minlength="1" required="required"
                        placeholder="lineId"/>
        </div>
    </div>

    <div class="row">
        <div id="cfnfxbn" class="form-group col-lg-4 col-lg-offset-4">
            <tr>Station Name</tr>
            <form:label path="name"/>
            <form:input path="name" minlength="1" required="required"
                        placeholder="name"/>
        </div>
    </div>

    </div>
    <div class="row">
        <div class="form-group col-lg-4 col-lg-offset-4">
            <button type="submit" class="btn btn-success">Add station</button>
        </div>
    </div>
</form:form>


</body>
</html>
