<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>/Good Old Railroad Inc./</h2>

<table class="sortable">
    <tr>
        Current user: ${sessionUser.login}
        <button onclick="location.href = '/logout'">Logout</button>
    </tr>
        <tr>
            <td>
                <button onclick="location.href = '/trains'">Trains</button>
            </td>
            <td>
                <button onclick="location.href = '/voyages'">Voyages</button>
            </td>
            <td>
                <button onclick="location.href = '/stations'">Stations</button>
            </td>
            <td>
                <button onclick="location.href = '/register'">Add new user</button>
            </td>
        </tr>
</table>

</body>
</html>







