<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>Trains</h2>

<table class="sortable">
    <c:forEach items="${trains}" var="train">
        <tr>
            <td>Train id: ${train.id}</td>
            <td>Train capacity: ${train.capacity}</td>
            <td>
                <button onclick="location.href = '/goodoldrailroad/train/${train.id}'">View Voyages</button>
            </td>
        </tr>
    </c:forEach>
</table>

<%--<c:url var="home" value="/goodoldrailroad"/>--%>
<%--<form:form action="${home}/trains" modelAttribute="train">--%>
    <%--<div class="row">--%>
        <%--<div class="form-group col-lg-4 col-lg-offset-4">--%>
            <%--<button type="submit" class="btn btn-success">Add train with capacity</button>--%>
        <%--</div>--%>
    <%--</div>--%>
    <%--<div class="row">--%>
        <%--<div id="form-group-email" class="form-group col-lg-4 col-lg-offset-4">--%>
            <%--<tr></tr>--%>
            <%--<form:label path="capacity"/>--%>
            <%--<form:input path="capacity" minlength="2" required="required"--%>
                        <%--placeholder="capacity"/>--%>
        <%--</div>--%>
    <%--</div>--%>
    <%--</div>--%>
<%--</form:form>--%>



</body>
</html>

