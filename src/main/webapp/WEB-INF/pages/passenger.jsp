<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>/Good Old Railroad Inc./</h2>
<table class="sortable">
    <tr>
        From: ${station1.name}
    </tr>
    <tr>
        <br/>
        To: ${station2.name}
    </tr>
    <tr>
        <td>Route name</td>
        <td>Voyage id </td>
        <td>Departure time</td>
    </tr>
    <tr>
        <br/>
        <td>${ttu.routeName}</td>
        <td>${ttu.voyageId}</td>
        <td>${ttu.departureTime}</td>
    </tr>
</table>
<br/>
<table>
    <c:url var="home" value="/"/>
    <form:form action="/buy/${station1.id}/${station2.id}/${voyage.id}/ticket" modelAttribute="passengerDto" method="post">
    <div class="row">
            <div id="passenger-name" class="form-group col-lg-4 col-lg-offset-4">
                <tr></tr>
                <form:label path="name"/>
                <form:input path="name" minlength="1" required="required"
                            placeholder="name"/>
            </div>
        </div>
        <div class="row">
            <div id="passenger-surname" class="form-group col-lg-4 col-lg-offset-4">
                <tr></tr>
                <form:label path="surname"/>
                <form:input path="surname" minlength="1" required="required"
                            placeholder="surname"/>
            </div>
        </div>
        <div class="row">
            <div id="passenger-birthdate" class="form-group col-lg-4 col-lg-offset-4">
                <tr></tr>
                <form:label path="birthdate"/>
                <form:input path="birthdate" minlength="8" required="required"
                            placeholder="birthdate"/>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-4 col-lg-offset-4">
                <button type="submit" class="btn btn-success">Confirm purchase</button>
            </div>
        </div>
    </form:form>
</table>
</body>
</html>