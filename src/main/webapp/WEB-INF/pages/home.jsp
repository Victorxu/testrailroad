<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>/Good Old Railroad Inc./</h2>
<table class="sortable">
    <c:forEach items="${stations}" var="station">
        <tr>
            <td>Line of station: ${station.line}</td>
            <td>Station name: ${station.name}</td>
            <td>
                <button onclick="location.href = '/schedule/${station.id}'">Schedule</button>
            </td>
            <td>
                <button onclick="location.href = '/buy/${station.id}'">Buy ticket</button>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>


<%--<table>--%>
<%--<form role = "form">--%>
    <%--<div class = "form-group">--%>
        <%--<label for = "train">Select list</label>--%>

        <%--<select class = "form-control">--%>
            <%--<option>1</option>--%>
            <%--<option>2</option>--%>
            <%--<option>3</option>--%>
            <%--<option>4</option>--%>
            <%--<option>5</option>--%>
        <%--</select>--%>
        <%--<label for = "name">Mutiple Select list</label>--%>
        <%--<select multiple class = "form-control">--%>
            <%--<option>1</option>--%>
            <%--<option>2</option>--%>
            <%--<option>3</option>--%>
            <%--<option>4</option>--%>
            <%--<option>5</option>--%>
        <%--</select>--%>
    <%--</div>--%>
<%--</form>--%>
<%--</table>--%>



<%--<c:url var="home" value="/home"/>--%>
<%--<form:form action="${home}" modelAttribute="train">--%>
    <%--<div class="row">--%>
        <%--<div class="form-group col-lg-4 col-lg-offset-4">--%>
            <%--<button type="submit" class="btn btn-success">Add train with capacity</button>--%>
        <%--</div>--%>
    <%--</div>--%>
    <%--<div class="row">--%>
        <%--<div id="form-group-email" class="form-group col-lg-4 col-lg-offset-4">--%>
            <%--<tr></tr>--%>
            <%--<form:label path="capacity"/>--%>
            <%--<form:input path="capacity" minlength="1" required="required"--%>
                        <%--placeholder="capacity"/>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</form:form>--%>











<%--<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>--%>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<%--<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>

<%--<html>--%>
<%--<head>--%>
<%--<title>Good Old Railroad Inc.</title>--%>
<%--</head>--%>
<%--<body>--%>

<%--<h2>/Good Old Railroad Inc./</h2>--%>

<%--<table class="sortable">--%>
<%--<tr>--%>
<%--<td>--%>
<%--<button onclick="location.href = '/goodoldrailroad/trains'">Trains</button>--%>
<%--</td>--%>
<%--</tr>--%>
<%--</table>--%>

<%--</body>--%>
<%--</html>--%>


<%--&lt;%&ndash;<td>ID:</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;<td>${train.capacity}</td>&ndash;%&gt;--%>

