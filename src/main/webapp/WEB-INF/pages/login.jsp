<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>/Good Old Railroad Inc./</h2>

<table class="sortable">
    <tr>
        <div class="container" align="center">
            <h2 align="center">Login</h2>

            <c:url var="getIn" value="/login"/>
            <form:form action="${getIn}" modelAttribute="loginUser">

                <div class="row" style="color: #545454;">
                    <div id="form-group-email" class="form-group col-lg-4 col-lg-offset-4">
                        <form:input path="login" minlength="4" maxlength="45" required="required"
                                    placeholder="Login"/>
                    </div>
                </div>

                <div class="row" style="color: #545454;">
                    <div id="form-group-password" class="form-group col-lg-4 col-lg-offset-4">
                        <form:password path="password" minlength="4" maxlength="45"
                                       required="required" placeholder="Password"/>
                    </div>
                </div>

                <div class="row" style="font-size: 15px; height: 25px; vertical-align: middle">
                    <div class="form-group col-lg-4 col-lg-offset-4" style="color: #545454;">
                        <input type="checkbox" onclick=
                                "if('text'==password.type)password.type='password';
                    else password.type='text';"/>
                        Show password</span>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-4 col-lg-offset-4">
                        <button type="submit" class="btn btn-success">Login</button>
                    </div>
                </div>
            </form:form>
        </div>



    </tr>
</table>

</body>
</html>







