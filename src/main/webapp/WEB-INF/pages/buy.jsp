<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>/Good Old Railroad Inc./</h2>
<table class="sortable">
    <tr>
        From: ${station1.name}
        <br/>
        To:
    </tr>
    <br/>
    <c:forEach items="${stations2}" var="station2">
        <tr>
            <td>${station2.name}</td>
            <td>
                <button onclick="location.href = '/buy/${station1.id}/${station2.id}'">Available voyages</button>
            </td>
        </tr>
    </c:forEach>

</table>
</body>
</html>