<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>/Good Old Railroad Inc./</h2>
<table class="sortable">
    <tr>
        <td>From station: ${station1.name}</td>
    </tr>
    <tr>
        <td>To station: ${station2.name}</td>
    </tr><br/>
    <td>
        <tr>
            <c:url var="timefilter" value="/buy/${station1.id}/${station2.id}"/>
            <form:form action="${timefilter}" modelAttribute="fromTimeToTimeDto">

        <tr>From time:</tr>
        <form:label path="fromTime"/>
        <form:input path="fromTime" minlength="1" required="required" placeholder="fromTime"/>
        <tr>To time:</tr>
        <form:label path="toTime"/>
        <form:input path="toTime" minlength="1" required="required" placeholder="toTime"/>
        <div class="row">
            <div class="form-group col-lg-4 col-lg-offset-4">
                <button type="submit" class="btn btn-success">Apply time filter</button>
            </div>
        </div>
        </form:form>
        </tr>
    </td>



    <br/>
    <c:forEach items="${timetable}" var="ttu">
        <tr>
            <td>${ttu.routeName}</td>
            <td>${ttu.voyageId}</td>
            <td>${ttu.departureTime}</td>
            <td>
                <button onclick="location.href = '/buy/${station1.id}/${station2.id}/${ttu.voyageId}'">Continue purchase</button>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>