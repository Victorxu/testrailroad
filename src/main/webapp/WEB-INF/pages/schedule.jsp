<%@ taglib prefix='form' uri='http://www.springframework.org/tags/form' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Old Railroad Inc.</title>
</head>
<body>

<h2>/Good Old Railroad Inc./</h2>
<table class="sortable">
    <tr>
        <td>Route name</td>
        <td>Voyage id </td>
        <td>Departure time</td>
    </tr>
    <br/>
    <c:forEach items="${timetable}" var="ttu">
        <tr>
            <td>${ttu.routeName}</td>
            <td>${ttu.voyageId}</td>
            <td>${ttu.departureTime}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>