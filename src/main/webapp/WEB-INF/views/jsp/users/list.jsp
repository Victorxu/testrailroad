<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp"/>

<body>

<div class="container">

    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>


    <%--<div class="alert alert-success" role="alert">--%>
    <%--<strong>Well done!</strong> You successfully read <a href="#" class="alert-link">this important alert message</a>.--%>
    <%--</div>--%>
    <%--<div class="alert alert-info" role="alert">--%>
    <%--<strong>Heads up!</strong> This <a href="#" class="alert-link">alert needs your attention</a>, but it's not super important.--%>
    <%--</div>--%>
    <%--<div class="alert alert-warning" role="alert">--%>
    <%--<strong>Warning!</strong> Better check yourself, you're <a href="#" class="alert-link">not looking too good</a>.--%>
    <%--</div>--%>
    <%--<div class="alert alert-danger" role="alert">--%>
    <%--<strong>Oh snap!</strong> <a href="#" class="alert-link">Change a few things up</a> and try submitting again.--%>
    <%--</div>--%>

    <h1>All Users</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>#ID</th>
            <th>Login</th>
            <th>Role</th>
        </tr>
        </thead>

        <c:forEach var="user" items="${users}">
            <tr>
                <td>${user.id}</td>
                <td>${user.login}</td>
                <td>${user.role}</td>

                <td>
                    <spring:url value="/users/${user.id}" var="userUrl"/>
                    <spring:url value="/users/${user.id}/delete" var="deleteUrl"/>
                    <spring:url value="/users/${user.id}/update" var="updateUrl"/>

                    <button class="btn btn-info" onclick="location.href='${userUrl}'">Show</button>
                    <button class="btn btn-primary" onclick="location.href='${updateUrl}'">Update</button>
                    <button class="btn btn-danger" onclick="this.disabled=true;post('${deleteUrl}')">Delete</button>
                </td>
            </tr>
        </c:forEach>
    </table>

    <%--<!-- Provides extra visual weight and identifies the primary action in a set of buttons -->--%>
    <%--<button type="button" class="btn btn-primary">Primary</button>--%>

    <%--<!-- Secondary, outline button -->--%>
    <%--<button type="button" class="btn btn-secondary">Secondary</button>--%>

    <%--<!-- Indicates a successful or positive action -->--%>
    <%--<button type="button" class="btn btn-success">Success</button>--%>

    <%--<!-- Contextual button for informational alert messages -->--%>
    <%--<button type="button" class="btn btn-info">Info</button>--%>

    <%--<!-- Indicates caution should be taken with this action -->--%>
    <%--<button type="button" class="btn btn-warning">Warning</button>--%>

    <%--<!-- Indicates a dangerous or potentially negative action -->--%>
    <%--<button type="button" class="btn btn-danger">Danger</button>--%>

    <%--<!-- Deemphasize a button by making it look like a link while maintaining button behavior -->--%>
    <%--<button type="button" class="btn btn-link">Link</button>--%>


</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>