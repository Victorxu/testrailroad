package com.tsystems.javaschool.goodoldrailroad.dao.impl;

import com.tsystems.javaschool.goodoldrailroad.TestHelper.TestHelper;
import com.tsystems.javaschool.goodoldrailroad.dao.api.LineDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LineDaoTest {

    private LineDao instance;

    @Before
    public void setup() throws Exception {
        instance = TestHelper.getInstance(getClass());
    }

    @Test(timeout = 500L)
    public void getAllLinesTest() {
        System.out.println(instance);
        System.out.println(instance.getClass().getName());
        Assert.assertFalse("Get all lines test failed", instance.getAllLines().isEmpty());
    }

}